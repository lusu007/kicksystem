package me.lusu007.kicksystem;

import de.slikey.effectlib.EffectLib;
import de.slikey.effectlib.EffectManager;
import de.slikey.effectlib.effect.BleedEffect;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Lukas on 14.02.2015.
 */
public class Main extends JavaPlugin implements Listener {

    public static EffectManager em;

    String message;

    public void onEnable() {

        em = new EffectManager(EffectLib.instance());

        this.getLogger().info("KickSystem aktiviert!");
    }

    public void onDisable() {
        this.getLogger().info("KickSystem deaktiviert!");
    }


    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player) sender;

        if(cmd.getName().equalsIgnoreCase("kick")) {
            if(p.hasPermission("server.kick.player")) {
                if (args.length >= 2) {
                    Player target = Bukkit.getPlayer(args[0]);

                    if(target != null && target.getName() != p.getName()) {


                            StringBuilder sb = new StringBuilder();
                            for (int i = 1; i < args.length; i++) {
                                sb.append(args[i]).append(" ");
                            }
                            String message = ChatColor.translateAlternateColorCodes('&', sb.toString());

                            BleedEffect effect1 = new BleedEffect(em);
                            effect1.duration = 30;
                            effect1.iterations = 2 * 20;
                            effect1.setLocation(target.getLocation());
                            effect1.start();

                            target.kickPlayer(ChatColor.DARK_RED + "Grund: §f" + message);

                    } else {
                        p.sendMessage("§6Du kannst §3dich §6nicht selber kicken!");
                    }
                } else {
                    p.sendMessage("§6/kick [Player] [Kickmessage]");
                }
            } else {
                p.sendMessage(" ");
            }
        }

        return true;
    }

    public void onKick(PlayerKickEvent e) {
        e.setLeaveMessage(null);
    }
}
